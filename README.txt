h1. Readme for $name

h2. credits
By Bèr Kessels, webschuur.com ber@webschuur.com
For Kaisu.org
First released for Drupal 4.7
[Homepage](http://drupal.org/project/refine_by_taxo)

h2. About
refine by taxo is another taxonomy browsing mechanism. It is best explained with
an example: You have five posts All of which are tagged (have taxonomy terms):

 * Cara Pils      tincan, beer, cheap
 * Duvel          bottle, beer, expensive
 * Water          cheap
 * Wine           bottle, expensive
 * Coffee         mug, cheap

Lets assume someone looks at a list of all items tagged 'cheap' (this is built-in Drupal functionality and not offered by this module). She would see the posts:

 * Cara Pils
 * Water
 * Coffee

Refine by taxonomy will offer a sideblock now, that presents common tags, to refine your list: In outr example we would see refine links for:

  beer (would show her all "cheap beer")
  mug (would shos her all "cheap drinks in mugs")

A refine link lives in a sideblock. It is nothing more then a link to a page wich has better filtered posts.
There are loads of options and tweakings possible, in total there are already 6 common blocks, differing in the way they filter (OR or AND), the way they look up relations (above aexample is 'trough nodes', but it is possible to set relations in the taxonomy interface, this is another block).

 -readme written in markdown-